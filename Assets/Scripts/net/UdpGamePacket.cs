﻿using UnityEngine;
using System.Collections;

public class UdpGamePacket<C> where C : ICommand {

    private const string UDP_ID = "js21";

    public string UdpID;
    public string ViewerID;
    public string SessionID;
    public string PacketID;

    public C Command;

    public string[][] getPackObject()
    {
        string[] udpInfo = new string[]{UdpID, ViewerID, SessionID, PacketID};
        string[][] str = new string[][]{udpInfo, Command.getPackObject()};
        return str;
    }
    //packet structure [UdpID, ViewerID, SessionID, PacketID, [CommandName, Param1, Param2]]

}
