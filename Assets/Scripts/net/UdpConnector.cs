﻿using UnityEngine;
using System.Collections;

public class UdpConnector  {

    private static UdpConnector instance;

    private UdpReceive udpReceive;
    private UdpSend udpSend;

    private UdpConnector()
    {
    }

    public static UdpConnector Instance
    {
        get{
            if (instance == null)
            {
                instance = new UdpConnector();
            }
            return instance;
        }
    }

    public void Init()
    {
        udpReceive = new UdpReceive();
        udpReceive.Start();

        udpSend = new UdpSend();
        udpSend.Init();
    }

    public void Stop()
    {
        udpReceive.Stop();
        udpSend.Stop();
    }

    public void Send(string message)
    {
        udpSend.Send(message);
    }

    public void Send(byte[] data)
    {
        udpSend.Send(data);
    }
}
