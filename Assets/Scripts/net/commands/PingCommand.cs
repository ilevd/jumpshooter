﻿using UnityEngine;
using System.Collections;

public class PingCommand : ICommand {

    public string Name;

    public string[] getPackObject()
    {
        return new string[] { Name };
    }
}
