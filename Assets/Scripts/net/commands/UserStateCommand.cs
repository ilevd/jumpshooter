﻿using UnityEngine;
using System.Collections;

public class UserStateCommand : ICommand {

    public string Name;
    public float Y;
    public float X;
    public float VY;
    public float VX;
    public float A;

    public string[] getPackObject()
    {
        return new string[] { Name, Y.ToString(), X.ToString(),
            VY.ToString(), VX.ToString(), A.ToString() };
    }
}
