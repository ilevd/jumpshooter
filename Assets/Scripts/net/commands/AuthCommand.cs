﻿using UnityEngine;
using System.Collections;

public class AuthCommand : ICommand {

    public string Name;
    public string ViewerID;
    public string AuthKey;
    public string SN;

    public string[] getPackObject()
    {
        return new string[] { Name, ViewerID, AuthKey, SN };
    }
}
