﻿using UnityEngine;
using System.Collections;
using System.Net.Sockets;
using System.Net;
using System;
using System.Threading;

public class UdpReceive  {


    private UdpClient udpClient;
    private Thread recvThread;

    public bool runReceive = true;

	// Use this for initialization
	public void Start ()
    {
        recvThread = new Thread(new ThreadStart(ReceiveData));
        recvThread.IsBackground = true;
        recvThread.Start();
	}

    public void Stop()
    {
        runReceive = false;
        recvThread.Abort();
        udpClient.Close();
    }

    private void ReceiveData()
    {
        Debug.Log("UdpReceive start receive: " + UdpSettings.CLIENT_PORT);

        udpClient = new UdpClient();
        udpClient.ExclusiveAddressUse = udpClient.Client.ExclusiveAddressUse = false;
        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        udpClient.Client.Bind(new IPEndPoint(IPAddress.Loopback, UdpSettings.CLIENT_PORT));
        
        while (runReceive)
        {
            try
            {
                IPEndPoint serverEndP = new IPEndPoint(IPAddress.Any, 0);
                byte[] data = udpClient.Receive(ref serverEndP);
                
                string text = System.Text.Encoding.UTF8.GetString(data);
                Debug.Log("UdpReceive from: " + serverEndP + " >> " + text);
            }
            catch (Exception err)
            {
                Debug.Log("UdpReceive receiveData error: " + err.ToString());
            }
        }
        udpClient.Close();
    }

}
