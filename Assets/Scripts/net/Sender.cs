﻿using UnityEngine;
using System.Collections;

public class Sender {

    public void sendAuth(string authKey, string sn, string viewerID) {
        AuthCommand command = new AuthCommand();
        command.Name = "auth";
        command.AuthKey = authKey;
        command.SN = sn;
        command.ViewerID = viewerID;
        send(new UdpGamePacket<AuthCommand>(), command);
    }

    public void sendState(float y, float x, float vy, float vx, float a) {
        UserStateCommand command = new UserStateCommand();
        command.Name = "user_state";
        command.Y = y;
        command.X = x;
        command.VX = vx;
        command.VY = vy;
        command.A = a;
        send(new UdpGamePacket<UserStateCommand>(), command);
    }

    public void sendPing() {
        PingCommand command = new PingCommand();
        command.Name = "ping";
        send(new UdpGamePacket<PingCommand>(), command);
    }

    private void send<C>(UdpGamePacket<C> packet, C command) where C:ICommand {
        setPacket(packet);
        packet.Command = command;
        MsgPack.ObjectPacker packer = new MsgPack.ObjectPacker();
        byte[] data = packer.Pack(packet.getPackObject());
        Debug.Log("Pack: " + data.Length + " " + System.Text.Encoding.UTF8.GetString(data));
        UdpConnector.Instance.Send(data);
    }

    private void setPacket<C>(UdpGamePacket<C> packet) where C:ICommand {
        packet.UdpID = UdpSettings.UDP_KEY;
        packet.ViewerID = "1039055";
        packet.SessionID = "4235";
        packet.PacketID = "235";
    }

    class Person {
        public string name = "igor";
        public int age = 10;
    }

    private void msgpack() {
        //MsgPack.CompiledPacker pp = new MsgPack.CompiledPacker();
        /*Person prs = new Person();
       byte[] data = p.Pack(prs);
       Person prs2 = p.Unpack<Person>(data);
       Debug.Log("Unp: " + prs2.name + " " + prs2.age);
       */
        /*string[] buf = new string[1024*32];
        for (int i = 0; i < 1024*32; i++)
        {
            buf[i] = "asdf";
        }
        byte[] data = p.Pack(buf);*/
    }


}
