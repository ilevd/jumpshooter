﻿using UnityEngine;
using System.Collections;
using System.Net;
using System;
using System.Net.Sockets;

public class UdpSend {

    Socket sock;
    UdpClient udpClient;
    IPEndPoint serverEndp;

	// Use this for initialization
	public void Init() {
        //setSocket();
        setServerEndP();
        setUdpClient();
	}


    /*private void setSocket()
    {
        sock = new Socket(AddressFamily.InterNetwork,
            SocketType.Dgram, ProtocolType.Udp);
        sock.ExclusiveAddressUse = false;
        sock.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        //sock.EnableBroadcast = true;
        sock.Bind(new IPEndPoint(IPAddress.Loopback, 0));//UdpSettings.CLIENT_PORT));
    }*/

    private void setUdpClient()
    {
        udpClient = new UdpClient();
        udpClient.ExclusiveAddressUse = udpClient.Client.ExclusiveAddressUse = false;
        udpClient.Client.SetSocketOption(SocketOptionLevel.Socket, SocketOptionName.ReuseAddress, true);
        udpClient.Client.Bind(new IPEndPoint(IPAddress.Loopback, UdpSettings.CLIENT_PORT));
    }

    private void setServerEndP()
    {
        IPAddress addr = IPAddress.Loopback;
        serverEndp = new IPEndPoint(addr, UdpSettings.SERVER_PORT);
    }

    public void Stop()
    {
        udpClient.Close();
    }

    public void Send(String message)
    {
        byte[] sendBytes = System.Text.Encoding.UTF8.GetBytes(message);
        Send(sendBytes);

    }

    public void Send(byte[] sendBytes)
    {
        try
        {
            Debug.Log("UdpClient: start sending");
            //sock.SendTo(sendBytes, serverEndp);
            udpClient.Send(sendBytes, sendBytes.Length, serverEndp);
        }
        catch (Exception e)
        {
            Debug.Log(e);
        }

    }


}
