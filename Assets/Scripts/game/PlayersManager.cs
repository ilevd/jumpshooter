﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayersManager : MonoBehaviour {

    private List<GameObject> players = new List<GameObject>();

    public List<PlayerInfo> getPlayersInfo() {
        List<PlayerInfo> plInfos = new List<PlayerInfo>();
        foreach (GameObject gm in players) {
            Player plScript = gm.GetComponent<Player>();
            plInfos.Add(plScript.PlayerInfo);
        }
        return plInfos;
    }

    public GameObject getPlayer(string id){
        for (int i = 0; i < players.Count; i++) {
            if (players[i].GetComponent<Player>().PlayerInfo.id == id) {
                return players[i];
            }
        }
        return null;
    }


    public GameObject addPlayer(PlayerInfo playerInfo, float xPos = 50, float yPos = -50) {
        GameObject player = getPlayer(playerInfo.id);
        if (player == null) {
            player = (GameObject) Instantiate(ResManager.Instance.player, new Vector3(xPos, yPos, 0), Quaternion.identity);
            Player playerSc = player.GetComponent<Player>();
            playerSc.PlayerInfo = playerInfo;
            players.Add(player);
        }
        return player;
    }


    public void deletePlayer()
    {

    }

    public void movePlayer(float y, float x, float vy, float vx, float a)
    {

    }

    public void moveX(string id, float x)
    {
        GameObject player = getPlayer(id);
        Player plScript = player.GetComponent<Player>();
        plScript.setXSpeed(x);
    }

    public void jump(string id)
    {
        GameObject player = getPlayer(id);
        Player plScript = player.GetComponent<Player>();
        plScript.jump();
    }


	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}
