﻿using UnityEngine;
using System.Collections;
using System;

public class Levels : MonoBehaviour {

    private static string[] LEVELS = new string[1] 
        { "29-35-36s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s33 2s14 15s4 2s16 11s6 2s7 4s21 3s6 6s19 4s5 8s17 5s4 10s15 40s" };



    public static char[,] getLevel(int level)
    {
        return getLevel(LEVELS[level]);
    }

    private static char[,] getLevel(string map)
    {
        //string map = "25-32-32s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s30 s20 s18 6s6 s20 s7 4s19 s20 s30 s20 s12 6s12 s20 s24 3s3 s20 s30 s20 s20 4s6 s20 s30 s20 s16 4s10 s20 s30 s20 s9 4s17 s20 s30 s20 32s644 ";
        string[] splitArr = map.Split(new char[] { '-' });
        foreach (string s in splitArr)
        {
            print(s);
        }
        int y = int.Parse(splitArr[0]);
        int x = int.Parse(splitArr[1]);
        map = splitArr[2];

        char[,] charArr = new char[y, x];
        int k = 0, m = 0, yy, xx;
        char ch;

        for (int i = 0; i < map.Length; i++)
        {
            k = 0;
            for (; Char.IsDigit(map[i]) && i < map.Length; i++)
            {
                if (k == 0) k = int.Parse(map[i].ToString());
                else
                {
                    k *= 10;
                    k += int.Parse(map[i].ToString());
                }
            }
            //print("Digit" + k.ToString());
            ch = map[i];
            if (k == 0) k++;
            for (int j = 0; j < k; j++)
            {
                yy = (int)(m / x);
                xx = (int)(m % x);
                //print("MapCoder: " + yy + " " + xx);
                charArr[yy, xx] = ch;
                m++;
            }
            //print("MapCoder2: " + yy + " " + xx );
        }
        return charArr;
    }

  
}
