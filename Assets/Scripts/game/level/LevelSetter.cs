﻿using UnityEngine;
using System.Collections;

public class LevelSetter : MonoBehaviour {

	// Use this for initialization
	void Start () {
        Debug.Log("LevelSetter: Start");
        setLevel();
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    //public GameObject wall;

    public void setLevel()
    {
        Debug.Log("SetLevel");
        char[,] level = Levels.getLevel(0);
        for (int i = 0; i < level.GetLength(0); i++)
        {
            for (int j = 0; j < level.GetLength(1); j++)
            {
                if (level[i, j] != ' ')
                {
                    //Instantiate(Resources.Load<GameObject>("Assets/Prefabs/Wall"), new Vector3(j * 20, -i * 20, 0), Quaternion.identity);
                    Instantiate(ResManager.Instance.wall, new Vector3(j * 20, -i * 20, 0), Quaternion.identity);
                }
            }
        }
    }
}
