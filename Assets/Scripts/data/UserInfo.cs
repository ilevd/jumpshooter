﻿using UnityEngine;
using System.Collections;

public class UserInfo  {

    private static UserInfo instance;

    public string id;
    public string firstName;
    public string lastName;
    public int sn;
    public int bd;

    private UserInfo(){
    }

    public static UserInfo I
    {
        get {
            if (instance == null)
            {
                instance = new UserInfo();
            }
            return instance;
        }
    }

    public void initTest()
    {
        id = "1039055";
        firstName = "Igor";
        lastName = "Levdanskiy";
    }
}
