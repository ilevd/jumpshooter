﻿using UnityEngine;
using System.Collections;

[ExecuteInEditMode]
public class CameraSize : MonoBehaviour {

    private const float CAMERA_Z = -50;

    private float lastW;
    private float lastH;


    void Awake()
    {
        setCameraSize();
    }

	// Use this for initialization
	void Start () {
        Debug.Log("CameraSize: Start");
        setCameraSize();
    }
	
	// Update is called once per frame
	void Update () {
        setCameraSize();
	}

    private void setCameraSize()
    {
        if (lastW != Screen.width || lastH != Screen.height)
        {
            lastW = Screen.width;
            lastH = Screen.height;
            Camera.main.orthographicSize = Screen.height / 2;
            Camera.main.transform.position = new Vector3(lastW / 2, -lastH / 2, CAMERA_Z);
        }
    }
}
