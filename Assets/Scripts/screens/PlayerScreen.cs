﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlayerScreen : MonoBehaviour {

    //public GUIStyle buttonStyle;
    //public GUISkin myGuiSkin;

    private bool draw;

    private List<PlayerInfo> players = new List<PlayerInfo>();

    public List<PlayerInfo> Players{
        set { players = value; }
        get{return players;}
    }

    public void show(List<PlayerInfo> players) {
        this.players = players;
        draw = true;
    }

    public void hide() {
        draw = false;
    }

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnGUI()
    {
        if (!draw) {
            return;
        }
        int boardWidth = 400;
        int boardHeight = 240;
        int left = (Screen.width - boardWidth) / 2;
        int top = (Screen.height - boardHeight) / 2;

        //GUI.skin = myGuiSkin;

        GUI.Box(new Rect(left, top, boardWidth, boardHeight),
        new GUIContent("Board", "tool"));

        int i = 0;
        foreach(PlayerInfo player in players){
           ++i;
           GUI.Label(new Rect(left + 20, top + 20 + i * 20, 100, 20), player.firstName);
        }
    }
}
