﻿using System.Collections;
using UnityEngine;

namespace Screens {

    public class ScreenItem {

        private GameObject _container;

        public void Init() {
            Debug.Log("Screen item init");
            _container = new GameObject();
            _container.AddComponent<Login>();
        }

        public GameObject getContainer() {
            return _container;
        }
    }


    internal sealed class Login : MonoBehaviour {

        void Start() {
            Debug.Log("Screen item init");
        }

        void Update() {
        }

        void OnGUI() {
            //GUI.Box(new Rect(10, 10, 100, 90), "Loader Menu");
            int boardWidth = 400;
            int boardHeight = 240;
            int left = (Screen.width - boardWidth) / 2;
            int top = (Screen.height - boardHeight) / 2;

            GUI.Box(new Rect(left, top, boardWidth, boardHeight),
                new GUIContent("Board", "tool"));

            for (int i = 0; i < 10; i++)
            {
                Color c = new Color();
                GUI.contentColor = new Color(0.1f * i, 0.1f * i, 0.1f * i, 1); // = #00FF00 + i * #0F;
                /*if (i == 0) {
                    GUI.contentColor = Color.green;
                }
                if (i == 1) {
                    GUI.contentColor = Color.red;
                }*/
                GUI.Label(new Rect(left, top + i* 20, 100, 20), "Player " + i.ToString());
            }

            GUI.Label(new Rect(100, 100, 40, 20), GUI.tooltip, "box");

            if (GUI.Button(new Rect(10, 10, 100, 20), "Button"))
            {
                Debug.Log("Button click");
            }
     
            //GUI.Box(new Rect(0, 0, 100, 50), "Top-left");
            GUI.Box(new Rect(Screen.width - 100, 0, 100, 50), "Top-right");
            GUI.Box(new Rect(0, Screen.height - 50, 100, 50), "Bottom-left");
            GUI.Box(new Rect(Screen.width - 100, Screen.height - 50, 100, 50), "Bottom-right");
        }

    }

}