﻿using UnityEngine;
using System.Collections;
using System;

public class Controls : MonoBehaviour {

    private const float XSPEED = 60;
    
    //private GameObject player;


    public event Action Jump = delegate {};
    public event Action<float> ChangeXSpeed = delegate {};
    public event Action<bool> TabPress = delegate { };

    private bool leftKeyDown = false;
    private bool rightKeyDown = false;
    private bool tabDown = false;


    /*public void setPlayer(GameObject player)
    {
        this.player = player;
    }*/

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        //float inputX = Input.GetAxis("Horizontal");
        //float inputY = Input.GetAxis("Vertical");

        if (Input.GetKeyDown(KeyCode.Tab) && tabDown == false) {
            Debug.Log("Tab!");
            tabDown = true;
            TabPress(true);
        }
        if(Input.GetKeyUp(KeyCode.Tab) && tabDown == true) {
            Debug.Log("Tab! =)");
            tabDown = false;
            TabPress(false);
        }

        if (Input.GetKeyDown(KeyCode.LeftArrow))
        {
            if (leftKeyDown == false)
            {
                Debug.Log("Controls leftKeyDown");
                leftKeyDown = true;
                changeXSpeed(-XSPEED);
            }
        }

        if (Input.GetKeyDown(KeyCode.RightArrow))
        {
            if (rightKeyDown == false)
            {
                rightKeyDown = true;
                changeXSpeed(XSPEED);
            }
        }


        if (Input.GetKeyUp(KeyCode.LeftArrow))
        {
            if (leftKeyDown == true)
            {
                leftKeyDown = false;
                Debug.Log("Controls leftKeyUp");
                if (rightKeyDown)
                {
                    changeXSpeed(XSPEED);
                }
                else
                {
                    changeXSpeed(0);
                }
            }
        }


        if (Input.GetKeyUp(KeyCode.RightArrow))
        {
            if (rightKeyDown == true)
            {
                rightKeyDown = false;
                if (leftKeyDown)
                {
                    changeXSpeed(-XSPEED);
                }
                else
                {
                    changeXSpeed(0);
                }
            }
        }

        if (Input.GetKeyDown(KeyCode.Space)) {
            Jump();
        }
	}

    private void changeXSpeed(float xSpeed)
    {
        ChangeXSpeed(xSpeed);
        //Player plScript = player.GetComponent<Player>();
        //plScript.setXSpeed(xSpeed);
    }

    private void jump()
    {
        Jump();
        //Player plScript = player.GetComponent<Player>();
        //plScript.jump();
    }

}
