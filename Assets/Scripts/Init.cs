﻿using UnityEngine;
using System.Collections;
using Screens;

namespace JumpShooter {

    public class Init : MonoBehaviour {

        public PlayersManager playerManager;
        public PlayerScreen playerScreen; 

        void Start() {
            Debug.Log("Init: START");
            Application.runInBackground = true;
            setGame();
        }

        private void setGame() {
            UserInfo.I.initTest();

            CameraSize cameraSize = gameObject.AddComponent<CameraSize>();

            LevelSetter levelSetter = gameObject.AddComponent<LevelSetter>();

            playerManager = gameObject.AddComponent<PlayersManager>();


            PlayerInfo plInfo = new PlayerInfo();
            plInfo.id = "1039090";
            plInfo.firstName = "Igor Lev";
           

            playerManager.addPlayer(plInfo);

            PlayerInfo plInfo2 = new PlayerInfo();
            plInfo2.id = "1039070";
            plInfo2.firstName = "Max Odintsov";

            playerManager.addPlayer(plInfo2, 80);

            Controls controls = gameObject.AddComponent<Controls>();
            controls.Jump += onJump;
            controls.ChangeXSpeed += onChangeXSpeed;
            controls.TabPress += onTabPress;
            //controls.player = GameObject.Find("Player");

            playerScreen = gameObject.AddComponent<PlayerScreen>();
         

            //ScreenItem si = new ScreenItem();
            //si.Init();

            UdpConnector.Instance.Init();
        }

        void OnDestroy() {
            Debug.Log("Init: STOP");
            UdpConnector.Instance.Stop();
        }

        // Update is called once per frame
        void Update() {

        }


        private void onJump()
        {
            playerManager.jump("1039090");
            playerManager.jump("1039070");
        }


        private void onChangeXSpeed(float xSpeed)
        {
            playerManager.moveX("1039090", xSpeed);
            playerManager.moveX("1039070", xSpeed);
        }


        private void onTabPress(bool isPress) {
            if (isPress) {
                playerScreen.show(playerManager.getPlayersInfo());
            } else {
                playerScreen.hide();
            }
        }
    }
}