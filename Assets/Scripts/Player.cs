﻿using UnityEngine;
using System.Collections;

public class Player : MonoBehaviour {

    private PlayerInfo playerInfo;
    private float xSpeed = 0;

    public PlayerInfo PlayerInfo
    {
        get {
            if (playerInfo == null)
            {
                playerInfo = new PlayerInfo();
            }
            return playerInfo;
        }
        set { playerInfo = value; }
    }

    // Use this for initialization
    void Start()
    {
        //playerInfo = new PlayerInfo();
    }


    // Update is called once per frame
    void Update()
    {
        move(xSpeed);
    }

    public void setXSpeed(float xSpeed)
    {
        this.xSpeed = xSpeed;
    }

    public void move(float horizontal)
    {
        rigidbody2D.velocity = new Vector2(xSpeed, rigidbody2D.velocity.y);
    }

    public void jump()
    {
        rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, 5400);
    }


 
}
