﻿using UnityEngine;
using System.Collections;

public class ResManager : MonoBehaviour {

    private static ResManager instance;

    public GameObject wall;
    public GameObject player;


    public static ResManager Instance {
        get {
            return instance;
        }
    }


	void Start () {
        if (instance == null) {
            instance = this;
        }
	}

	void Update () {
	}

}
